#include <stdio.h>

#include "buffer.h"

static int tests = 0;
static int fails = 0;

static int buffer[MAXLEN] = {0}; //Testing buffer

#define TEST(condition, ...) \
    tests++;\
    if (!(condition))\
    {\
        fails++;\
        printf("Error: ");\
        printf(__VA_ARGS__);\
        printf("\n");\
    }

#define PRINT_TEST_REPORT\
    printf("%d tests performed: %d succeded, %d failed.\n", tests, tests - fails, fails);

int main(void)
{
	int i = 0;

    // test if the buffer is empty at startup
    TEST(buffer_is_empty(), "buffer not empty at startup")
    // test if the buffer is not full at startup
    TEST(!buffer_is_full(), "buffer full at startup")

	//Print data buffer
	printf("Data buffer: ");
	printf("[");
	for(i = 0; i < MAXLEN ; i++){
		printf("%d", buffer[i]);
		if(i != (MAXLEN-1)){
			printf(",");
		}
	}
	printf("]\r\n");
	i = 1;

    //Fill buffer until it is full
    while(buffer_put(i++));
	i = 0;

    TEST(buffer_is_full(), "buffer not full after put failed")

    //Read buffer until it is empty
    while(buffer_get(&buffer[i++]));
    i = 0;

    TEST(buffer_is_empty(), "buffer not empty after get failed")

	//Print data buffer
	printf("Data buffer (filled): ");
	printf("[");
	for(i = 0; i < MAXLEN ; i++){
		printf("%d", buffer[i]);
		if(i != (MAXLEN-1)){
			printf(",");
		}
	}
	printf("]\r\n");

	//Test correct value placement
	for(i = 0 ; i < MAXLEN ; i++){
		TEST(buffer[i] = (i+1), "buffer item is not the right value: item [%d] = %d, expected value = %d\r\n", i, buffer[i], (i+1))
	}

	//Circular test
	for(i = 0			; i < (MAXLEN / 2) 						  ; i++)buffer_put(i);
	for(int j = 0		; j < (MAXLEN / 4) 						  ; j++)buffer_get(&buffer[j]);
	for(i = (MAXLEN / 2); i < ( (MAXLEN / 4 * 3) + (MAXLEN / 2) ) ; i++)buffer_put(i);

	i = 0;
	while(buffer_get(&buffer[i])){ //Read the entire buffer
		TEST(buffer[i] = (i + MAXLEN / 4), "buffer item is not the right value: item [%d] = %d, expected value = %d\r\n", i, buffer[i], (i + MAXLEN / 4))
		i++;
	}
	for(i = 0; i < MAXLEN - 1 ; i++){
		TEST(buffer[i] = (buffer[i+1]-1), "buffer item is not the right value: item [%d] = %d, expected value = %d\r\n", i, buffer[i], (buffer[i+1]-1))
	}

	//Print data buffer
	printf("Data buffer (filled): ");
	printf("[");
	for(i = 0; i < MAXLEN ; i++){
		printf("%d", buffer[i]);
		if(i != (MAXLEN-1)){
			printf(",");
		}
	}
	printf("]\r\n");


    PRINT_TEST_REPORT
    return 0;
}
