#include <stddef.h>
#include "buffer.h"

// implementation for a buffer with ints


typedef struct buffer_element_tag{
	int value;
	struct buffer_element_tag *next;
}buffer_element;

// shared variables within this file
buffer_element *head = NULL; //Write counter of buffer
buffer_element *tail = NULL; //Read counter of buffer
buffer_element *first = NULL; //Address first buffer item
buffer_element *last = NULL; //Address first buffer item
//buffer_element *newNode = {NULL, NULL};


static int extern_buffer[MAXLEN] = {0}; //Data buffer

static bool full = false;

bool buffer_put(int p)
{
	buffer_element *newNode = malloc(sizeof(buffer_element)); //Create mem for new item
	newNode->value = p; //Set value
	newNode->next = NULL; //Set address next item (=unknown)
	if(head == NULL){
		head = tail = newNode;
	} else {
		tail = tail->next = newNode;
	}
	return true;
}

bool buffer_get(int *p)
{
	if(head == tail){
		printf("error: no new items found (first = last)\r\n");
		return false;
	} else {
		*p = head->value; //Store value of buffer item
		buffer_element *tempNode = head; //Temporarily store initial address head
		head = head->next; //Advance head to next buffer item location
		free(first); //Free initial memory (has served its purpose)
		return true;
	}
}

bool buffer_is_full(void)
{
    return full;
}

bool buffer_is_empty(void)
{
    return !full;
}
