#include <stdio.h>
#include <stdbool.h>

#define MAXLEN 8

static int head = 0; //Write counter of buffer
static int tail = 0; //Read counter of buffer

static int extern_buffer[MAXLEN] = {0}; //Data buffer
static int buffer[MAXLEN] = {0}; //Testing buffer

static bool full = false;

/*
 * Writes a new value into the buffer
 * (unless buffer is full)
 *
 * returns: true on successful write
 * 		    false when buffer is full
 */
bool D_write(int p){
	if(full){
		printf("No action: buffer is already full\r\n");
		return false; //Buffer is full
	}
	extern_buffer[head%MAXLEN] = p;
	head++;
	if(tail == ((head)%MAXLEN)){
		full = true;
	} else {

	}
	/*
	if(head > MAXLEN){
		tail++;
	}
	*/
	return true;
}

/*
 * Reads values from buffer
 * (unless no more new items)
 */
bool D_read(int *p){
	if(tail == head && !full){
		printf("No action: buffer is empty\r\n");
		return false; //Buffer is empty (no new values)
	}
	*p = extern_buffer[tail%MAXLEN];
	tail++;
	if(full){
		full = false;
	}
	return true;
}


int main(void){
	int i = 0;
	printf(">Start\r\n");

	//Print data buffer
	printf("Data buffer: ");
	printf("[");
	for(i = 0; i < MAXLEN ; i++){
		printf("%d", buffer[i]);
		if(i != (MAXLEN-1)){
			printf(",");
		}
	}
	printf("]\r\n");

	//Fill buffer
	for(i = 0; i < MAXLEN ; i++){
		D_write(i);
	}

	for(i = 0 ; i < MAXLEN+2 ; i++){
		D_read(&buffer[i]);
	}

	//Print data buffer
	printf("Data buffer (filled): ");
	printf("[");
	for(i = 0; i < MAXLEN ; i++){
		printf("%d", buffer[i]);
		if(i != (MAXLEN-1)){
			printf(",");
		}
	}
	printf("]\r\n");

	printf(">Finish")

	return 0;
}
