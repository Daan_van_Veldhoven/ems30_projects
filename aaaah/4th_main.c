/* Very simple queue
 * These are FIFO queues which discard the new data when full.
 *
 * Queue is empty when in == out.
 * If in != out, then
 *  - items are placed into in before incrementing in
 *  - items are removed from out before incrementing out
 * Queue is full when in == (out-1 + QUEUE_SIZE) % QUEUE_SIZE;
 *
 * The queue will hold QUEUE_ELEMENTS number of items before the
 * calls to QueuePut fail.
 */

/* Queue structure */
#include <stdio.h>
#include <stdbool.h>

#define QUEUE_SIZE 8
int Queue[QUEUE_SIZE] = {0};
int Read[QUEUE_SIZE] = {0};
int QueueIn, QueueOut;

void QueueInit(void)
{
    QueueIn = QueueOut = 0;
}

int QueuePut(int new)
{
    Queue[QueueIn%QUEUE_SIZE] = new;

    QueueIn = (QueueIn + 1);
    if(QueueIn > QUEUE_SIZE){
    	QueueOut++;
    }

    return 0; // No errors
}

int QueueGet(int *old)
{
    if(QueueIn == QueueOut)
    {
        return -1; /* Queue Empty - nothing to get*/
    }

    *old = Queue[QueueOut%QUEUE_SIZE];

    QueueOut = ((QueueOut + 1)%QUEUE_SIZE);

    return 0; // No errors
}

int main(void){
    for(int i = 0; i < QUEUE_SIZE;i++)
    {
        printf(",%d",Read[i]);
    }

    //put
    for(int i = 0; i < 10;i++)
    {
        QueuePut(i);
    }

    for(int i = 0; i < 8;i++)
    {
        QueueGet(&Read[i]);
    }
    printf("\n");
    for(int i = 0; i < QUEUE_SIZE;i++)
    {
        printf(",%d",Read[i]);
    }
}
