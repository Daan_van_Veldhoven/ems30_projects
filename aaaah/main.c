/*
 * main.c
 *
 *  Created on: 21 Apr 2021
 *      Author: daanv
 */

#include <stdio.h>
#include <stdbool.h>

#define BUFLEN 4

static int buffer[BUFLEN] = {4,5,6,7};

bool addrTest(int* p, int index){
	if(index < BUFLEN){
		*p = buffer[index];
		return true;
	} else {
		return false;
	}



}


int main(void){
	int ret_val;
	printf("Initial ret_val: %d\r\n"
			"Initial addr ret_val: 0x%x\r\n\n", ret_val, &ret_val);

	addrTest(&ret_val, 0);
	printf("New base ret_val[0]: %d\r\n"
		   "New (base) addr ret_val: 0x%x\r\n", ret_val, &ret_val);

	addrTest(&ret_val, 3);
	printf("ret_val[3]: %d\r\n"
		   "ret_val[3] addr: 0x%x", ret_val, &ret_val);

}
