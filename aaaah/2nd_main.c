#include <stdio.h>
#include <stdbool.h>



#define        SIZE_OF_BUFFER            8    // Maximum size of buffer

int     storeBuffer[SIZE_OF_BUFFER] = { 0 };    // Empty circular buffer in buffer.c
int     readBuffer[SIZE_OF_BUFFER] = { 0 };    // the one in test.c
int		readIndex  =    0;    // Index of the read pointer
int 	writeIndex =    0;    // Index of the write pointer

bool full = false;

void printStatus()
{
    printf("\nbuffer[%d,%d,%d,%d,%d,%d,%d,%d]\nwriteIndex = %d\n\n"
            ,readBuffer[0],readBuffer[1],readBuffer[2],readBuffer[3],readBuffer[4],
            readBuffer[5],readBuffer[6],readBuffer[7],writeIndex);
}

void buffer_put(int val){
    storeBuffer[writeIndex] = val;
    writeIndex++;
    if(writeIndex == SIZE_OF_BUFFER){
        writeIndex = 0;
        full = true;
    }
}

void buffer_get(int *a){

    if(full){
        //readIndex = (writeIndex) % SIZE_OF_BUFFER;
        full = false;
    }
    *a = storeBuffer[readIndex];
    readIndex = (readIndex + 1) % SIZE_OF_BUFFER;
}

int main(void){

    //display buffer
    printf(">Initializing");
    printStatus(readBuffer);

    int i = 0;
    int *ptr_arr[SIZE_OF_BUFFER] = {0};

    //fill buffer
    printf(">Filling buffer");
    for(i = 0; i < 10; i++){
        buffer_put(i);
    }
    printf("\n Filling completed\n\n>Reading buffer");
    //read buffer
    for(i = 0; i < 8; i++){
    	buffer_get(&readBuffer[readIndex]);
    }
    printStatus();
    function();

}
