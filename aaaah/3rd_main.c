#include <stdio.h>

const int MAX = 3;
#define MAXLEN  3

int  storeBuffer[MAXLEN] = {1,2,3};
int  *readBuffer[MAXLEN] = {0};

int main (void)
{

   int i;

   for ( i = 0; i < MAX; i++) {
       readBuffer[i] = &storeBuffer[i]; /* assign the address of integer. */
   }

   for ( i = 0; i < MAX; i++) {
      printf("buffer[%d] = %d\n", i, *readBuffer[i] );
   }

   return 0;
}
